
# Architecture
The Template driven approach was used to build a platform agnostic integration layer which provides running asset servers for differrent languages for different platforms.

![img](./img/create.png)

# Quick Start Guide

1. Install caso pip package

> pip install git+https://gitlab.com/smartproductionsystems/projects/caso/caso-cli@v2

2. Initialize new Project

> caso init --name robot
> cd robot

A fresh project with the following structure will be created (see figure)

![img](./img/init.png)

3. Create an Asset File
Asset Files are the topological structure of an industrial asset using the caso meta model

![img](./img/asset.png)

4. Generate OPC UA server based on asset files

> caso asset create robot.asset --template opcuaAsset.py

The server will be created in the build directory of the asset folder

Templates are provided by the templates folder and are typically shipped by third parties

5. Run the OPC UA Server

> caso asset run assets/build/robot

What you get is a standardized runnable opcua server for python

![img](./img/server.png)



