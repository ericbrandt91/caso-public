import configparser
from opcua import ua,  Server
import time
#asset server
class Endpoints:
    def __init__(self):
        pass

    def endpoints(self,myobj,idx):
        id = 0
        rootAsset = myobj.add_object(ua.NodeId("Assets",idx), "Assets")
       
        {% for endpoint in model.functions%}
            {% if endpoint.type == 'append' %}
        root = rootAsset.add_object(ua.NodeId(str(id)+".{{endpoint.root}}",idx), "{{endpoint.root}}")
        id = id +1
                {% for model in endpoint.params %}                              
        main = root.add_object(ua.NodeId(str(id)+".{{model.name}}",idx), "{{model.name}}")
        id = id +1
                    {% for endpoint in model.endpoints %}
        endpoint = main.add_variable(ua.NodeId(str(id)+".{{endpoint}}",idx), "{{endpoint}}", 0.0)
        endpoint.set_writable()
        id = id +1
                    {% endfor %}
                {% endfor %}  
            {% endif %}      
        {% endfor %}


class AssetServer:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("asset.ini")
        self.server = Server()
        check = config.get('DEFAULT','url',fallback=False)
        if(check!=False):
           self.server.set_endpoint(config['DEFAULT']['url'])
        uri = "http://examples.freeopcua.github.io"
        self.idx = self.server.register_namespace(uri)
        objects = self.server.get_objects_node()
        # populating our address space
        self.myobj = objects.add_object(self.idx, "Root")
    
    def addEndpoints(self):
        e = Endpoints()
        e.endpoints(self.myobj,self.idx)

    def startAssetServer(self): 
        self.server.start()
        while True:
            time.sleep(0.01)        
    
assetserver = AssetServer()

assetserver.addEndpoints()
assetserver.startAssetServer()
