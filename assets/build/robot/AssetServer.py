import configparser
from opcua import ua,  Server
import time
#asset server
class Endpoints:
    def __init__(self):
        pass

    def endpoints(self,myobj,idx):
        id = 0
        rootAsset = myobj.add_object(ua.NodeId("Assets",idx), "Assets")
       
        
            
        root = rootAsset.add_object(ua.NodeId(str(id)+".Roboter",idx), "Roboter")
        id = id +1
                                              
        main = root.add_object(ua.NodeId(str(id)+".RoboterStatus",idx), "RoboterStatus")
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".Busy",idx), "Busy", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".Run",idx), "Run", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".Start",idx), "Start", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".byRetVal",idx), "byRetVal", 0.0)
        endpoint.set_writable()
        id = id +1
                    
                                              
        main = root.add_object(ua.NodeId(str(id)+".Laufband",idx), "Laufband")
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".QA4_A1",idx), "QA4_A1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".QA4_A2",idx), "QA4_A2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xH_BG1",idx), "xH_BG1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xH_BG2",idx), "xH_BG2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xG1_BG40",idx), "xG1_BG40", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xG_MB40",idx), "xG_MB40", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xQA1_A1",idx), "xQA1_A1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xQA2_A1",idx), "xQA2_A1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xQA3_A1",idx), "xQA3_A1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xG1_BG51",idx), "xG1_BG51", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".xG1_BG50",idx), "xG1_BG50", 0.0)
        endpoint.set_writable()
        id = id +1
                    
                                              
        main = root.add_object(ua.NodeId(str(id)+".RFID1",idx), "RFID1")
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".diONo1",idx), "diONo1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".diPNo1",idx), "diPNo1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iOPos1",idx), "iOPos1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iOpNo1",idx), "iOpNo1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iResourceId1",idx), "iResourceId1", 0.0)
        endpoint.set_writable()
        id = id +1
                    
                                              
        main = root.add_object(ua.NodeId(str(id)+".RFID2",idx), "RFID2")
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".diONo2",idx), "diONo2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".diPNo2",idx), "diPNo2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iOPos2",idx), "iOPos2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iOpNo2",idx), "iOpNo2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".iResourceId2",idx), "iResourceId2", 0.0)
        endpoint.set_writable()
        id = id +1
                    
                                              
        main = root.add_object(ua.NodeId(str(id)+".MES",idx), "MES")
        id = id +1
                    
        endpoint = main.add_variable(ua.NodeId(str(id)+".connected",idx), "connected", 0.0)
        endpoint.set_writable()
        id = id +1
                    
                  
                  
        


class AssetServer:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("asset.ini")
        self.server = Server()
        check = config.get('DEFAULT','url',fallback=False)
        if(check!=False):
           self.server.set_endpoint(config['DEFAULT']['url'])
        uri = "http://examples.freeopcua.github.io"
        self.idx = self.server.register_namespace(uri)
        objects = self.server.get_objects_node()
        # populating our address space
        self.myobj = objects.add_object(self.idx, "Root")
    
    def addEndpoints(self):
        e = Endpoints()
        e.endpoints(self.myobj,self.idx)

    def startAssetServer(self): 
        self.server.start()
        while True:
            time.sleep(0.01)        
    
assetserver = AssetServer()

assetserver.addEndpoints()
assetserver.startAssetServer()